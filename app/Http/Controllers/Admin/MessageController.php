<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Message;
use App\Http\Traits\FileTrait;

class MessageController extends Controller
{
    use FileTrait;

    public function index(Request $request)
    {
        $messages = Message::withTrashed()
                            ->whereTitle($request->title)
                            ->whereBody($request->body)
                            ->whereImage($request->imageOption)
                            ->whereDeleteStatus($request->statusOption)
                            ->sortFromNewest()
                            ->paginate(20);

        return view('admin.pages.message.index', compact('messages', 'request'));
    }

    public function massDestroy(Request $request)
    {
        $ids = explode(',', $request->ids);

        Message::destroy($ids);

        return redirect()->back();
    }

    public function destroy(Message $message)
    {
        if(!empty($message))
            $message->delete();

        $this->destroyPhoto($message);

        return redirect()->back();
    }

    public function destroyPhoto(Message $message)
    {
        if (!empty($message)) {
            $message->deletePhoto = true;
            $message->photo       = NULL;

            $message->save();

            $this->deleteFile($message->photo);
        }

        return redirect()->back();
    }

    public function restore(Message $message)
    {
        $message->restore();

        return redirect()->back();
    }
}
