<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegistrationRequest;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    public function confirmation(RegistrationRequest $request)
    {
        return view('auth.register_confirmation', compact('request'));
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(RegistrationRequest $request)
    {
        $user = User::create($request->all());

        event(new Registered($user));

        return redirect()->route('register.confirmation_email_sent');
    }

    public function showConfirmationEmailSent()
    {
        return view('auth.email_confirmation_sent');
    }

    public function showVerifyEmailSuccess()
    {
        return view('auth.verify_email_success');
    }
}
