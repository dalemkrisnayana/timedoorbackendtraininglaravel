<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\MessageRequest;
use Illuminate\Http\Request;
use App\Models\Message;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Hash;
use Illuminate\Support\Facades\Gate;
use App\Http\Traits\FileTrait;
use Auth;

class MessageController extends Controller
{
    use FileTrait;

    public function index()
    {
        $messages = Message::sortFromNewest()->paginate(10);

        return view('user.pages.message.index', compact('messages'));
    }

    public function store(MessageRequest $request)
    {
        if($request->validator->fails())
            return redirect()->route('user.message.all')
                             ->withErrors($request->validator->messages());

        $message = $request->all();

        if(Auth::check())
            $message += ['user_id' => Auth::user()->id];

        Message::create($message);

        return redirect()->route('user.message.all');
    }

    public function edit(Message $message, $currentPage, $passwordRequest = NULL)
    {
        if ($this->authenticate($passwordRequest, $message, Message::EDIT, $currentPage) != NULL)
            return $this->authenticate($passwordRequest, $message, Message::EDIT, $currentPage);

        return view('user.pages.message.update', [
            'messageItem'     => $message,
            'currentPage'     => $currentPage,
            'passwordRequest' => $passwordRequest
        ]);
    }

    public function update(MessageRequest $request, Message $message, $currentPage)
    {
        $this->authorize('passwordCheck', [$message, $request->password]);

        if($request->validator->fails()) return redirect()->route('user.message.edit', [
                $message,
                $currentPage,
                $request->password
            ])->withErrors($request->validator->messages());

        if (isset($request->delete_photo)) {
            $this->deleteFile($message->photo);
            $message->deletePhoto = true;
            $message->photo       = NULL;
        }

        $message->update($request->all());

        return redirect()->route('user.message.all', ['page' => $currentPage]);
    }

    public function receiver(Request $request, Message $message, $currentPage)
    {
        $action = $request->action;

        if ($action == Message::EDIT) {

            return $this->edit($message, $currentPage, $request->password);

        } else if ($action == Message::DELETE) {

            return $this->showDeleteConfirmation($message, $currentPage, $request->password);

        }
    }

    public function authenticate($passwordRequest, Message $message, $action, $currentPage)
    {
        if (Auth::check() && Auth::user()->id != $message->user_id || !Auth::check() && !Hash::check($passwordRequest, $message->password))
            return view('user.pages.message.auth_error', compact(
                'message',
                'currentPage',
                'action'
            ));
    }

    public function showDeleteConfirmation(Message $message, $currentPage, $passwordRequest = NULL)
    {
        if ($this->authenticate($passwordRequest, $message, Message::DELETE, $currentPage) != NULL) 
            return $this->authenticate($passwordRequest, $message, Message::DELETE, $currentPage);

        return view('user.pages.message.delete', compact(
            'currentPage',
            'message',
            'passwordRequest'
        ));
    }

    public function destroy(Message $message, $redirectPage, Request $request)
    {
        $this->authorize('passwordCheck', [$message, $request->password]);

        $this->deleteFile($message->photo);
        $message->delete();

        return redirect()->route('user.message.all', [
            'page' => $this->getRedirectPage($redirectPage)
        ]);
    }

    public function getRedirectPage($redirectPage)
    {
        $message = (new Message())->orderByCreatedAtPagination();

        if ($message->total() <= $redirectPage * $message->perPage()) return $redirectPage -= 1;

        return $redirectPage;
    }

}