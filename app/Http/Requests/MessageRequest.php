<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class MessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public $validator = null;

    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator) {
        $this->validator = $validator;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|min:10|max:32',
            'body'  => 'required|min:10|max:200',
            'photo' => 'mimes:jpeg,png,jpg,gif|max:1048',
            'name'  => 'nullable|min:3|max:16',
        ];

        if($this->method() == 'POST') {
            $rules += [
                'password' => 'min:4|max:4',
            ];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'title.min'      => 'Your title must be 10 to 32 characters long',
            'title.max'      => 'Your title must be 10 to 32 characters long',
            'body.required'  => 'A message is required',
            'body.min'       => 'Your body must be 10 to 200 characters long',
            'body.max'       => 'Your body must be 10 to 200 characters long',
            'password.min'   => 'Your password must be 4 digit number',
            'password.max'   => 'Your password must be 4 digit number',
            'photo.max'      => 'Your image is only valid 1MB or less',
            'photo.mimes'    => 'Your image is only valid jpeg, .jpg, .png, .gif',
            'name.min'       => 'Your name must be 3 to 16 characters long',
            'name.max'       => 'Your name must be 3 to 16 characters long',
        ];
    }
}
