<?php

namespace App\Http\Traits;
use \Illuminate\Support\Facades\Storage;

trait FileTrait {

    public function uploadFile($imageRequest)
    {
        $imageName = time() . '.' .$imageRequest->extension();
        Storage::putFileAs('/images', $imageRequest, $imageName);

        return $imageName;
    }

    public function deleteFile($fileName)
    {
        Storage::delete('images/' . $fileName);
    }

}