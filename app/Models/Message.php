<?php

namespace App\Models;

use App\Http\Traits\FileTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Hash;
use Auth;
use \Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use HasFactory;
    use FileTrait;
    use SoftDeletes;

    const DELETE = 'delete';
    const EDIT   = 'edit';

    public $deletePhoto = false;

    protected $fillable = [
        'name',
        'title',
        'body',
        'password',
        'photo',
        'user_id',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function setPhotoAttribute($value)
    {
        if ($this->deletePhoto) return $this->attributes['photo'] = NULL;

        $this->attributes['photo'] = $this->uploadFile($value);
    }

    public function scopeSortFromNewest($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }

    public function scopeWhereTitle($query, $title)
    {
        if (!empty($title))
            return $query->where('title', $title);
    }

    public function scopeWhereBody($query, $body)
    {
        if (!empty($body))
            return $query->where('body', $body);
    }

    public function scopeWhereImage($query, $option)
    {
        if ($option == 'with') {
            $query = $query->whereNotNull('photo');
        } else if ($option == 'without') {
            $query = $query->whereNull('photo');
        }

        return $query;
    }

    public function scopeWhereDeleteStatus($query, $option)
    {
        if ($option == 'delete') {
            $query = $query->whereNotNull('deleted_at');
        } else if ($option == 'on') { 
            $query = $query->whereNull('deleted_at');
        }

        return $query;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
