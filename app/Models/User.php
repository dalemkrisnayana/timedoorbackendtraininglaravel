<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory;
    use Notifiable;

    public $roles = [
        'user'  => 'user',
        'admin' => 'admin'
    ];

    protected $fillable = [
        'name',
        'email',
        'password'
    ];

    public function getIsAdminAttribute()
    {
        return ($this->attributes['role'] == $this->roles['admin']);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'user_id');
    }
}
