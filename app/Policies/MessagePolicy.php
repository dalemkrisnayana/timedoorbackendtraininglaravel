<?php

namespace App\Policies;

use App\Models\Message;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use Hash;
use Auth;

class MessagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function passwordCheck(?User $user, Message $message, $passwordRequest)
    {
        return (!Auth::check() && Hash::check($passwordRequest, $message->password) || Auth::check() && $message->user_id == Auth::user()->id);
    }
}
