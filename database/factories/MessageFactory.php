<?php

namespace Database\Factories;

use App\Models\Message;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Hash;

class MessageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Message::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'      => Str::random(10),
            'body'       => Str::random(10),
            'password'   => Hash::make('1234'),
            'name'       => Str::random(10),
            'updated_at' => now(),
            'created_at' => now()
        ];
    }
}
