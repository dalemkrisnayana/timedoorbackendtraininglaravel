@extends('layouts.admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12"><!-- /.col-xs-12 -->
                <div class="box box-success">
                <div class="box-header with-border">
                    <h1 class="font-18 m-0">Timedoor Challenge - Level 9</h1>
                </div>
                    <div class="box-body">
                        <div class="bordered-box mb-20">
                            <form class="form" role="form" method="GET">
                                <table class="table table-no-border mb-0">
                                    <tbody>
                                    <tr>
                                        <td width="80"><b>Title</b></td>
                                        <td><div class="form-group mb-0">
                                        <input type="text" class="form-control" name="title" value="{{ $request->title }}">
                                        </div></td>
                                    </tr>
                                    <tr>
                                        <td><b>Body</b></td>
                                        <td><div class="form-group mb-0">
                                        <input type="text" class="form-control" name="body" value="{{ $request->body }}">
                                        </div></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-search">
                                    <tbody>
                                    <tr>
                                        <td width="80"><b>Image</b></td>
                                        <td width="60">
                                        <label class="radio-inline">
                                            <input type="radio" name="imageOption" id="inlineRadio1" value="with" @if($request->imageOption == 'with') checked @endif> with
                                        </label>
                                        </td>
                                        <td width="80">
                                        <label class="radio-inline">
                                            <input type="radio" name="imageOption" id="inlineRadio2" value="without" @if($request->imageOption == 'without') checked @endif> without
                                        </label>
                                        </td>
                                        <td>
                                        <label class="radio-inline">
                                            <input type="radio" name="imageOption" id="imageRadio3" value="unspecified" @if($request->imageOption == 'unspecified' || empty($request->imageOption)) checked @endif> unspecified
                                        </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="80"><b>Status</b></td>
                                        <td>
                                        <label class="radio-inline">
                                            <input type="radio" name="statusOption" id="inlineRadio1" value="on" @if($request->statusOption == 'on') checked @endif> on
                                        </label>
                                        </td>
                                        <td>
                                        <label class="radio-inline">
                                            <input type="radio" name="statusOption" id="inlineRadio2" value="delete" @if($request->statusOption == 'delete') checked @endif> delete
                                        </label>
                                        </td>
                                        <td>
                                        <label class="radio-inline">
                                            <input type="radio" name="statusOption" id="statusRadio3" value="unspecified" @if($request->statusOption == 'unspecified' || empty($request->statusOption)) checked @endif> unspecified
                                        </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><button type="submit" class="btn btn-default mt-10"><i class="fa fa-search"></i> Search</button></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th><input type="checkbox" onclick="checkBoxParent(this)"></th>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Body</th>
                                <th width="200">Image</th>
                                <th>Date</th>
                                <th width="50">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($messages as $message)
                                    @if($message->trashed())
                                        <tr class="bg-gray-light">
                                            <td></td>
                                            <td>{{ $message->id }}</td>
                                            <td>{{ $message->title }}</td>
                                            <td>{{ $message->body }}</td>
                                            <td> - </td>
                                            <td>{{ $message->created_at->format('d-m-Y') }}<br><span class="small">{{ $message->created_at->format('H:m') }}</span></td>
                                            <td>
                                                <form method="POST" action="{{ route('admin.message.recover', $message) }}">
                                                    @csrf
                                                    <button type="submit" class="btn btn-default" rel="tooltip" title="Recover"><i class="fa fa-repeat"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td><input type="checkbox" value="{{ $message->id }}" name="deleteCheck" class="childCheckBox"></td>
                                            <td>{{ $message->id }}</td>
                                            <td>{{ $message->title }}</td>
                                            <td>{{ $message->body }}</td>
                                            <td>
                                                @empty($message->photo)
                                                    -
                                                @else
                                                    <img class="img-prev" src="{{ asset('images/' . $message->photo) }}" width="100" height="100">
                                                    <a onclick="deleteItem('{{ route('admin.message.deletePhoto', $message) }}')" class="btn btn-danger ml-10 btn-img" rel="tooltip" title="Delete Image"><i class="fa fa-trash"></i></a>
                                                @endempty
                                            </td>
                                            <td>{{ $message->created_at->format('d-m-Y') }}<br><span class="small">{{ $message->created_at->format('H:m') }}</span></td>
                                            <td><a onclick="deleteItem('{{ route('admin.message.delete', $message) }}')" class="btn btn-danger" rel="tooltip" title="Delete"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                        <a onclick="massDelete('{{ route('admin.message.massDelete') }}')" class="btn btn-default mt-5">Delete Checked Items</a>
                        <div class="text-center">
                            <nav>
                                {{ $messages->links('vendor.pagination.custom') }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div><!-- /.col-xs-12 -->
        </div>

        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <div class="text-center">
                    <h4 class="modal-title" id="myModalLabel">Delete Data</h4>
                  </div>
                </div>
                <div class="modal-body pad-20">
                  <p>Are you sure want to delete this item(s)?</p>
                </div>
                <div class="modal-footer">
                  <form action="" id="deleteForm" method="POST" style="margin: 0;">
    
                    @csrf
                    @method('DELETE')
            
                    <input type="hidden" value="" name="ids" id="messageIds"/>
                    <button type="button" class="btn btn-default" onclick="closeDeleteModal()">Close</button>
                    <button type="submit" class="btn btn-danger">Delete</button>
                  </form>
                </div>
              </div>
            </div>
          </div>

        </div>
    </section>
@endsection