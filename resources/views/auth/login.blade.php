@extends('layouts.user')

@section('content')
<div class="container" style="height: 120vh;">
    <div class="row">
        <div class="col">
            <div class="box login-box">
                <div class="login-box-head">
                  <h1 class="mb-5">{{ __('Login') }}</h1>
                  <p class="text-lgray">Please login to continue...</p>
                </div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="login-box-body">
                        <div class="form-group">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
                            
                            @error('email')
                                <p class="mt-5 small text-danger">*{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                            @error('password')
                                <p class="mt-5 small text-danger hide">*{{ $message }}</p>
                            @enderror
                            
                        </div>
                    </div>
                    <div class="login-box-footer">
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection