@extends('layouts.user')

@section('content')
    <div class="container" style="height: 120vh;">
        <div class="row">
            <div class="col">
                <div class="box login-box">
                    <div class="login-box-head">
                    <h1 class="mb-5">{{ __('Register') }}</h1>
                    <p class="text-lgray">Please fill the information below...</p>
                    </div>
                    <div class="login-box-body">
                        <form method="POST" action="{{ route('register.confirmation') }}">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name" @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="E-mail" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password" @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                            <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required autocomplete="new-password">
                            </div>
                            </div>
                            <div class="login-box-footer">
                            <div class="text-right">
                                <a href="{{ route('user.message.all') }}" class="btn btn-default">Back</a>
                                <button type="submit" class="btn btn-primary">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection