@extends('layouts.user')

@section('content')
<div class="container" style="height: 120vh;">
    <div class="row">
        <div class="col">
            <div class="box login-box">
                <div class="login-box-head">
                <h1 class="mb-5">{{ __('Register') }}</h1>
                </div>
                <div class="login-box-body">
                    <table class="table table-no-border">
                    <tbody>
                        <tr>
                        <th>Name</th>
                        <td>{{ $request->name }}</td>
                        </tr>
                        <tr>
                        <th>E-mail</th>
                        <td>{{ $request->email }}</td>
                        </tr>
                        <tr>
                        <th>Password</th>
                        <td>{{ $request->password }}</td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                <div class="login-box-footer">
                    <div class="text-right">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <input type="hidden" name="name" value="{{ $request->name }}">
                            <input type="hidden" name="email" value="{{ $request->email }}">
                            <input type="hidden" name="password" value="{{ $request->password }}">
                            <input type="hidden" name="password_confirmation" value="{{ $request->password_confirmation }}">

                            <button type="submit" class="btn btn-primary">Submit</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection