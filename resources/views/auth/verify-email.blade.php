@extends('layouts.user')

@section('content')
<div class="container" style="height: 120vh;">
    <div class="row">
        <div class="col">
            <div class="box login-box text-center">
                <div class="login-box-head">
                  <h1>Verify Email</h1>
                </div>
                <div class="login-box-body">
                  <p>{{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}</p>
                    @if (session('status') == 'verification-link-sent')
                        <div class="mb-4 font-medium text-sm text-green-600" style="color: #6DDA5F;">
                            {{ __('A new verification link has been sent to the email address you provided during registration.') }}
                        </div>
                    @endif
                </div>
                <div class="login-box-footer">
                    <div class="mt-4 flex items-center justify-between">
                        <form method="POST" action="{{ route('verification.send') }}">
                            @csrf
            
                            <div>
                                <button class="btn btn-primary">
                                    {{ __('Resend Verification Email') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
