@extends('layouts.user')

@section('content')
<div class="container" style="height: 120vh;">
    <div class="row">
        <div class="col">
            <div class="box login-box text-center">
                <div class="login-box-head">
                  <h1>Successfully Registered</h1>
                </div>
                <div class="login-box-body">
                  <p>Thank you for your registration. Membership is now complete.</p>
                </div>
                <div class="login-box-footer">
                  <div class="text-center">
                    <a href="{{ route('user.message.all') }}" class="btn btn-primary">Back to Home</a>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection