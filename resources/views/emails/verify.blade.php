@component('mail::message')

Hi, Mr {{ $notifiable['name'] }}

To enable your account, please click in the following link or copy it onto the address bar of your favourite browser.

<a href="{{ $url }}">{{ $url }}</a>

Please click within 24 hours.

Regards,<br>
{{ config('app.name') }}

@endcomponent