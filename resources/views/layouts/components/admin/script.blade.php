<!-- jQuery 3 -->
<script src="{{ asset('assets/admin/plugin/jquery/jquery.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('assets/admin/plugin/jquery/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/admin/plugin/bootstrap/bootstrap.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('assets/admin/plugin/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/admin/plugin/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('assets/admin/plugin/bootstrap-datepicker/bootstrap-datetimepicker.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/js/adminlte.min.js') }}"></script>
<!-- DataTable -->
<script src="{{ asset('assets/admin/plugin/datatable/datatables.min.js') }}"></script>
<!-- CKEditor -->
<script src="{{ asset('assets/admin/plugin/ckeditor/ckeditor.js') }}"></script>
<!-- Selectpicker -->
<script src="{{ asset('assets/admin/plugin/selectpicker/bootstrap-select.js') }}"></script>
<!-- Custom Delete Modal -->
<script>
    // Delete
    var deleteModal  = document.getElementById("deleteModal");
    var deleteForm   = document.getElementById("deleteForm");

    function deleteItem(action)
    {
        deleteModal.className     = "modal";
        deleteModal.style.display = "block";
        deleteForm.action         = action;
    }

    function closeDeleteModal()
    {
        deleteModal.className     = "modal fade";
        deleteModal.style.display = "none";
    }

    function massDelete(action)
    {
        var messageInputs         = document.getElementById("messageIds");
        var childCheckBoxes       = document.getElementsByClassName("childCheckBox");
        var checkedBoxes          = [];

        deleteModal.className     = "modal";
        deleteModal.style.display = "block";
        deleteForm.action         = action;

        for(var i = 0; i < childCheckBoxes.length; i++)
            if (childCheckBoxes[i].checked == true)
                checkedBoxes.push(childCheckBoxes[i].value);

        messageInputs.value = checkedBoxes;
    }
</script>

<!-- Custom Checkbox Show -->
<script>
    var childCheckBoxes  = document.getElementsByClassName("childCheckBox");

    function checkBoxParent(parent)
    {
        for(var i = 0; i < childCheckBoxes.length; i++)
            childCheckBoxes[i].checked = parent.checked;
    }
</script>