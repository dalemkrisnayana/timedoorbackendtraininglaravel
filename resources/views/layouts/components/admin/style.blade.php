<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{ asset('assets/admin/plugin/bootstrap/bootstrap.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('assets/admin/plugin/font-awesome/font-awesome.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('assets/admin/plugin/Ionicons/ionicons.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/admin.css') }}">
<!-- TMDR Preset -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/tmdrPreset.css') }}">
<!-- Custom css -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/custom.css') }}">
<!-- Skin -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/skin.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('assets/admin/plugin/bootstrap-datepicker/bootstrap-datetimepicker.min.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('assets/admin/plugin/daterangepicker/daterangepicker.css') }}">
<!-- DataTable -->
<link rel="stylesheet" href="{{ asset('assets/admin/plugin/datatable/datatables.min.css') }}">
<!-- DataTable -->
<link rel="stylesheet" href="{{ asset('assets/admin/plugin/selectpicker/bootstrap-select.css') }}">
<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">