<html>
    <head>
        <title>Timedoor Challenge - Level 8</title>
        @include('layouts.components.user.style')
        @include('layouts.components.user.script')
    </head>

    <body class="bg-lgray">
        <header>
            @include('layouts.components.user.header')
        </header>
         <main>
             <div class="section">
                  @yield('content')
              </div>
          </main>
    
        <footer>
            @include('layouts.components.user.footer')
        </footer>

        <script>
            // INPUT TYPE FILE
            $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            $(document).ready( function() {
                $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
                    var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }
                });
            });
         </script>
    </body>
</html>