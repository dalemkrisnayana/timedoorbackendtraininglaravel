@extends('user.pages.message.item', ['message' => $message])

@section('itemContent')
    @if(empty($message->password))
        <p class="small text-danger mt-5">{{ __('passwords.not_exist', ['action' => $action]) }}</p>

        <div style="text-align: center; padding: 10px 0;">
            <a class="btn btn-default" href="{{ route('user.message.all', ['page' => $currentPage]) }}" style="margin: 10px;">Back previous page</a>
        </div>
    @else
        <p class="small text-danger mt-5">{{ __('passwords.not_match') }}</p>

        <form class="form-inline mt-50" method="POST" action="{{ route('user.message.receiver', [$message, $currentPage]) }}">
        @csrf
            <div class="form-group mx-sm-3 mb-2">
                <label for="inputPassword2" class="sr-only">Password</label>
                <input type="password" class="form-control" id="inputPassword2" placeholder="Password" name="password">
            </div>
            @if($action == "delete")
                <button type="submit" name="action" value="delete" class="btn btn-danger mb-2"><i class="fa fa-trash p-3"></i></button>
            @elseif($action == "edit")
                <button type="submit" name="action" value="edit" class="btn btn-default mb-2"><i class="fa fa-pencil p-3"></i></button>
            @endif
        </form>
    @endif
@endsection