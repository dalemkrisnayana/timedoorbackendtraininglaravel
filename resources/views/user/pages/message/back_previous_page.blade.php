@extends('user.pages.message.item', ['passwordErrors' => $passwordErrors, 'message' => $message])

@section('itemContent')
    <div style="text-align: center; padding: 10px 0;">
        <a class="btn btn-default" href="{{ route('user.message.all', ['page' => $currentPage]) }}" style="margin: 10px;">Back previous page</a>
    </div>
@endsection