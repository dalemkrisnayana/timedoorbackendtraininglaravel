@extends('user.pages.message.item', ['message' => $message])

@section('itemContent')
    <div class="row">
        <div class="col-md-4">
            <img class="img-responsive" alt="" src="{{ asset('assets/upload_image/' . $message->photo) }}">
        </div>
    </div>
    <div style="text-align: center; padding: 10px 0;">
        <span>Are you sure?</span><br>
        <form action="{{ route('user.message.destroy', [$message, $currentPage]) }}" method="POST" style="margin: 0;">

        @csrf
        @method('DELETE')

            <input type="hidden" value="{{ $passwordRequest }}" name="password"/>
            <button type="submit" class="btn btn-danger" style="margin: 10px;">Yes</button>
            <a class="btn btn-default" href="{{ route('user.message.all', ['page' => $currentPage]) }}" style="margin: 10px;">Cancel</a>
        </form>
    </div>
@endsection
