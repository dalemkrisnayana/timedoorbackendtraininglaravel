@extends('layouts.user')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
                <form action="{{ route('user.message.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="text-center">
                        <h1 class="text-green mb-30"><b>Level 8 Challenge</b></h1>
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" @guest value="{{ old('name') }}" @else value="{{ Auth::user()->name }}" @endguest>
                        @error('name')
                            <p class="small text-danger mt-5">*{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                        @error('title')
                            <p class="small text-danger mt-5">*{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Body</label>
                        <textarea rows="5" class="form-control" name="body">{{ old('body') }}</textarea>
                        @error('body')
                            <p class="small text-danger mt-5">*{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Choose image from your computer :</label>
                        <div class="input-group">
                            <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file">
                                    <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="photo" multiple>
                                </span>
                            </span>
                        </div>
                        @error('photo')
                            <p class="small text-danger mt-5">*{{ $message }}</p>
                        @enderror
                    </div>
                    @guest
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password">
                            @error('password')
                                <p class="small text-danger mt-5">*{{ $message }}</p>
                            @enderror
                        </div>
                    @endguest
                    <div class="text-center mt-30 mb-30">
                      <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
                <hr>

                @foreach($messages as $message)
                    <div class="post">
                        <div class="clearfix">
                            <div class="pull-left">
                                <h2 class="mb-5 text-green"><b>{{ $message->title }}</b></h2>
                            </div>
                            <div class="pull-right text-right">
                                <p class="text-lgray">{{ $message->created_at->format('d-m-Y') }}<br/><span class="small">{{ $message->created_at->format('H:m') }}</span></p>
                            </div>
                        </div>
                        <h4 class="mb-20">
                            @empty($message->name)
                                No Name
                            @else
                                {{ $message->name }}
                            @endempty

                            <span class="text-id">-</span>

                            @if(!empty($message->user_id))
                                [ID: {{ $message->user->id }}]
                            @endif
                        </h4>
                        <p>{{ $message->body }}</p>
                        <div class="img-box my-10">
                            <img class="img-responsive img-post" src="{{ asset('images/' . $message->photo) }}" alt="image">
                        </div>
                        <form class="form-inline mt-50" method="POST" action="{{ route('user.message.receiver', [$message, $messages->currentPage()]) }}">
                            @csrf
                            <div class="form-group mx-sm-3 mb-2">
                                @if(!Auth::check() && empty($message->user_id))
                                    <label for="inputPassword2" class="sr-only">Password</label>
                                    <input type="password" class="form-control" id="inputPassword2" placeholder="Password" name="password">
                                @endif
                            </div>
                            @if(!Auth::check() && empty($message->user_id) || Auth::check() && $message->user_id == Auth::user()->id)
                                <button type="submit" name="action" value="edit" class="btn btn-default mb-2"><i class="fa fa-pencil p-3"></i></button>
                                <button type="submit" name="action" value="delete" class="btn btn-danger mb-2"><i class="fa fa-trash p-3"></i></button>
                            @endif
                        </form>
                    </div>
                @endforeach

                <div class="text-center mt-30">
                    <nav>
                        {{ $messages->links('vendor.pagination.custom') }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection