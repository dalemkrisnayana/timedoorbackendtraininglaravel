@extends('layouts.user')

@section('content')
    <div class="container" style="height: 120vh;">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
                <div class="post">
                    <div class="clearfix">
                        <div class="pull-left">
                            <h2 class="mb-5 text-green"><b>{{ $message->title }}</b></h2>
                        </div>
                        <div class="pull-right text-right">
                            <p class="text-lgray">{{ $message->created_at->format('d-m-Y') }}<br/><span class="small">{{ $message->created_at->format('H:m') }}</span></p>
                        </div>
                    </div>

                    <h4 class="mb-20">
                        @empty($message->name)
                            No Name
                        @else
                            {{ $message->name }}
                        @endempty

                        <span class="text-id">-</span>

                        @if(!empty($message->user_id))
                            [ID: {{ $message->user->id }}]
                        @endif
                    </h4>
                    <p>{{ $message->body }}</p>

                    @yield('itemContent')

                </div>
            </div>
        </div>
    </div>
@endsection