@extends('layouts.user')

@section('content')
    <div class="container" style="height: 120vh;">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
                <div class="post">
                    
                    @error('name')
                        <p class="small text-danger mt-5">*{{ $message }}</p>
                    @enderror
                    @error('title')
                        <p class="small text-danger mt-5">*{{ $message }}</p>
                    @enderror
                    @error('body')
                        <p class="small text-danger mt-5">*{{ $message }}</p>
                    @enderror
                    @error('photo')
                        <p class="small text-danger mt-5">*{{ $message }}</p>
                    @enderror

                    <form action="{{ route('user.message.update', [$messageItem, $currentPage]) }}" method="POST" enctype="multipart/form-data">

                    @csrf
                    @method('PUT')

                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $messageItem->name }}">
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" name="title" value="{{ $messageItem->title }}">
                            </div>
                            <div class="form-group">
                                <label>Body</label>
                                <textarea rows="5" class="form-control" name="body">{{ $messageItem->body }}</textarea>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <img class="img-responsive" alt="" src="{{ asset('images/' . $messageItem->photo) }}">
                                </div>
                                <div class="col-md-8 pl-0">
                                    <label>Choose image from your computer :</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control upload-form" value="{{ $messageItem->photo }}" readonly>
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="photo" multiple>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="delete_photo">Delete image
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="password" value="{{ $passwordRequest }}"/>
                        </div>
                        <div class="modal-footer" style="display: flex; justify-content: center; border: 0;">
                            <a href="{{ route('user.message.all', ['page' => $currentPage]) }}" class="btn btn-default" data-dismiss="modal">Cancel</a>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection