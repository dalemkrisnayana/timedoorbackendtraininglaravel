@if ($paginator->hasPages())
    <ul class="pagination">
        @if ($paginator->onFirstPage())
            <li class="disabled"><span>&laquo;</span></li>
            <li class="disabled"><span>&lsaquo;</span></li>
        @else
            <li><a href="{{ $paginator->url(1) }}">&laquo;</a></li>
            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&lsaquo;</a></li>
        @endif

        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif
            
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><span>{{ $page }}</span></li>
                    @else
                        @if($paginator->currentPage() >= 3 && $page > $paginator->currentPage() - 3 && $page < $paginator->currentPage() + 3)
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @elseif($paginator->currentPage() < 3 && $page <= 5)
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @elseif($paginator->currentPage() > $paginator->lastPage() - 3 && $page >= $paginator->lastPage() - 5)
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endif
                @endforeach
            @endif
        @endforeach

        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">&rsaquo;</a></li>
            <li><a href="{{ $paginator->url($paginator->lastPage()) }}">&raquo;</a></li>
        @else
            <li class="disabled"><span>&rsaquo;</span></li>
            <li class="disabled"><span>&raquo;</span></li>
        @endif
    </ul>
@endif