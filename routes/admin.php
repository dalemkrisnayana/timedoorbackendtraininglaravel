<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'as' => 'message.',
], function () {

    Route::get('/', 'MessageController@index')
                    ->name('all');
    Route::delete('/delete/{message}', 'MessageController@destroy')
                    ->name('delete');
    Route::delete('/delete_photo/{message}', 'MessageController@destroyPhoto')
                    ->name('deletePhoto');
    Route::delete('/mass_delete', 'MessageController@massDestroy')
                    ->name('massDelete');
    Route::post('/recover/{message}', 'MessageController@restore')
                    ->name('recover');

});