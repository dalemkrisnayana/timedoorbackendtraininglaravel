<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('user.message.all');
})->name('home');

Route::group([
    'as'         => 'message.',
    'middleware' => 'guest.verified',
], function () {

    Route::get('/', 'MessageController@index')
                    ->name('all');
    Route::post('/store', 'MessageController@store')
                    ->name('store');

    Route::get('/edit/{message}/{currentPage}/{password?}', 'MessageController@edit')
                    ->name('edit');
    Route::put('/update/{message}/{currentPage}', 'MessageController@update')
                    ->name('update');

    Route::post('/receiver/{message}/{currentPage}', 'MessageController@receiver')
                    ->name('receiver');

    Route::get('/delete/{message}/{currentPage}/{password?}', 'MessageController@showDeleteConfirmation')
                    ->name('delete');
    Route::delete('/destroy/{message}/{currentPage}', 'MessageController@destroy')
                    ->name('destroy');

});

